package db

import (
	"context"
	"log"
	"time"

	"gitlab.com/zlyzol/dexarbot/ctx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ArbitrageOppCheckResult struct {
	BidNdx  int     `json:"bidNdx"`
	AskNdx  int     `json:"askNdx"`
	BidLeft float64 `json:"bidLeft"`
	AskLeft float64 `json:"askLeft"`
}
type SimpleOrderbookEntry struct {
	Price  float64 `json:"price"`
	Amount float64 `json:"amount"`
}
type OrderbookEntry struct {
	Normal SimpleOrderbookEntry `json:"normal"`
	Ex     SimpleOrderbookEntry `json:"ex"`
}
type Orderbook struct {
	Bids []OrderbookEntry `json:"bids"`
	Asks []OrderbookEntry `json:"asks"`
}
type TradeDebug struct {
	ArbRes     ArbitrageOppCheckResult `json:"arbRes"`
	PriceVol1  SimpleOrderbookEntry                `json:"pricevol1"`
	PriceVol2  SimpleOrderbookEntry                `json:"pricevol2"`
	PriceVol3  SimpleOrderbookEntry                `json:"pricevol3"`
	Trade      [3]SimpleOrderbookEntry `json:"trade"`
	Orderbook1 Orderbook               `json:"orderbook1"`
	Orderbook2 Orderbook               `json:"orderbook2"`
	Orderbook3 Orderbook               `json:"orderbook3"`
	OrderbookX Orderbook               `json:"orderbookX"`
}
type Trade struct {
	Time      time.Time  `json:"time"`
	Asset     string     `json:"asset"`
	AmountIn  float64    `json:"amountIn"`
	AmountOut float64    `json:"amountOut"`
	PNL       float64    `json:"pnl"`
	Debug     TradeDebug `json:"debug"`
}

func GetTrades(ctx *ctx.Context, limit int64) ([]*Trade, error) {
	collection := ctx.Db.Database("test").Collection("trades")
	findOptions := options.Find()
	findOptions.SetLimit(limit)
	findOptions.SetSort(bson.D{{"time", -1}})
	results := make([]*Trade, 0, 100)
	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Printf("ERROR: DB Find trade failed: %v", err)
		return nil, err
	}
	for cur.Next(context.TODO()) {
		var elem Trade
		err := cur.Decode(&elem)
		if err != nil {
			log.Printf("ERROR: DB Find trade failed: %v", err)
			return nil, err
		}
		results = append(results, &elem)
	}
	if err := cur.Err(); err != nil {
		log.Printf("ERROR: DB Find trade failed: %v", err)
		return nil, err
	}
	cur.Close(context.TODO())
	return results, nil
}

func InsertTrade(ctx *ctx.Context, trade *Trade) error {
	collection := ctx.Db.Database("test").Collection("trades")
	_, err := collection.InsertOne(context.TODO(), trade)
	if err != nil {
		log.Printf("ERROR: DB Insert trade failed: %v", err)
	}
	return err
}
