package db

import (
	"context"
	"log"

	"gitlab.com/zlyzol/dexarbot/ctx"
	"go.mongodb.org/mongo-driver/bson"
)

type Stats struct {
	TradeCount  uint    `json:"tradeCount"`
	AvgPNL      float64 `json:"avgPNL"`
	TotalVolume float64 `json:"totalVolume"`
	AvgTrade    float64 `json:"avgTrade"`
	TimeRunning string  `json:"timeRunning"`
}

func GetStatsx(ctx *ctx.Context) (Stats, error) {
	stats := Stats{
		TradeCount:  1,
		TotalVolume: 3,
		AvgTrade:    4,
		AvgPNL:      5,
		TimeRunning: "6",
	}
	return stats, nil
}

func GetStats(ctx *ctx.Context) (*Stats, error) {
	c := ctx.Db.Database("test").Collection("trades")
	pipe := []bson.M{
		{"$group": bson.M{
			"_id":         "",
			"totalVolume": bson.M{"$sum": "$amountin"},
			"AvgPNL":      bson.M{"$avg": "$pnl"},
			"avgTrade":    bson.M{"$avg": "$amountin"},
			"tradeCount":  bson.M{"$sum": 1},
		}},
	}
	cur, err := c.Aggregate(context.TODO(), pipe)
	result := Stats{}
	if err != nil {
		log.Printf("ERROR: DB Get stats failed: %v", err)
		return nil, err
	}
	if cur.Next(context.TODO()) {
		var elem Stats
		err := cur.Decode(&elem)
		if err != nil {
			log.Printf("ERROR: DB Get stats failed: %v", err)
			return nil, err
		}
		result = elem
	}
	if err := cur.Err(); err != nil {
		log.Printf("ERROR: DB Get stats failed: %v", err)
		return nil, err
	}
	cur.Close(context.TODO())

	return &result, nil
}
