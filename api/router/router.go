package router

import (
	//"gitlab.com/zlyzol/dexarbot/bot"
	"net/http"
    "github.com/go-chi/chi"
	"gitlab.com/zlyzol/dexarbot/api/server"
)

func New(srv *server.Server) *chi.Mux {
	r := chi.NewRouter()
	// Routes for APIs
	r.Route("/api/v1", func(r chi.Router) {
		r.Use(ContentTypeJson)

		r.MethodFunc("GET", "/", srv.HandleApiIndex)
		
		// Routes for healthz
		r.Get("/healthy/liveness", server.HandleLive)
		r.MethodFunc("GET", "/healthz/readiness", srv.HandleReady)
		
		// Routes for bot
		r.MethodFunc("POST", "/start", srv.HandleBotStart)
		r.MethodFunc("POST", "/stop", srv.HandleBotStop)
		r.MethodFunc("GET", "/status", srv.HandleBotStatus)
		r.MethodFunc("GET", "/stats", srv.HandleBotStats)
		r.MethodFunc("GET", "/logs", srv.HandleBotLogs)
		r.MethodFunc("GET", "/logs/{limit}", srv.HandleBotLogs)
		r.MethodFunc("GET", "/trades", srv.HandleBotTrades)
		r.MethodFunc("GET", "/trades/{limit}", srv.HandleBotTrades)
	})
	r.Route("/", func(r chi.Router) {
		r.MethodFunc("GET", "/", srv.HandleMainIndex)
	})
	return r
}

func ContentTypeJson(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        w.Header().Set("Content-Type", "application/json;charset=utf8")
        next.ServeHTTP(w, r)
    })
}

