package bot

import (
	"fmt"
	"log"
	"math"
	"sync/atomic"
	"time"

	"gitlab.com/zlyzol/dexarbot/bot/order"
	"gitlab.com/zlyzol/dexarbot/bot/orderbook"
	"gitlab.com/zlyzol/dexarbot/ctx"
	"gitlab.com/zlyzol/dexarbot/db"
)

type Bot struct {
	ctx                        *ctx.Context
	running                    bool
	StartTime                  time.Time
	busy                       int32
	obTB, obTC, obCB, obMerged *orderbook.Orderbook
	asobTB, asobTC, asobCB     *orderbook.AssetOrderbook
	acc                        *ctx.Account
	refresh                    chan struct{}
}

func New(ctx *ctx.Context) *Bot {
	bot := Bot{
		ctx: ctx,
		acc: ctx.GetAcc(),
	}
	return &bot
}
func (bot *Bot) Ping() error {
	return nil
}
func (bot *Bot) Start() {
	if !bot.running {
		bot.ctx.Start()
		bot.running = true
		bot.StartTime = time.Now()

		bot.asobTB = orderbook.NewAssetOrderbook(bot.ctx, bot.ctx.Config.TradedAsset+"_"+bot.ctx.Config.BaseAsset)  // RUNE-B1A_BUSD-BD1
		bot.asobCB = orderbook.NewAssetOrderbook(bot.ctx, bot.ctx.Config.CrossAsset+"_"+bot.ctx.Config.BaseAsset)   // BNB_BUSD-BD1
		bot.asobTC = orderbook.NewAssetOrderbook(bot.ctx, bot.ctx.Config.TradedAsset+"_"+bot.ctx.Config.CrossAsset) // RUNE-B1A_BNB

		//bot.debugPath()
		go bot.Loop()
	}
}
func (bot *Bot) Stop() {
	if bot.running {
		bot.ctx.Stop()
		bot.running = false
	}
}
func (bot *Bot) GetStatus() string {
	if bot.running {
		return "running..."
	} else {
		return "stopped"
	}
}
func (bot *Bot) GetStats() string {
	s := "stats"
	return s
}
func (bot *Bot) GetLogs(limit uint64) string {
	s := "logs"
	return s
}
func (bot *Bot) debugPath() {

	bot.obTB = orderbook.DebugNewAssetOrderbook(bot.ctx, bot.ctx.Config.TradedAsset+"_"+bot.ctx.Config.BaseAsset)  // RUNE-B1A_BUSD-BD1
	bot.obCB = orderbook.DebugNewAssetOrderbook(bot.ctx, bot.ctx.Config.CrossAsset+"_"+bot.ctx.Config.BaseAsset)   // BNB_BUSD-BD1
	bot.obTC = orderbook.DebugNewAssetOrderbook(bot.ctx, bot.ctx.Config.TradedAsset+"_"+bot.ctx.Config.CrossAsset) // RUNE-B1A_BNB

	/*
		bot.obTB, bot.obCB, bot.obTC = GetTestOrderbooks5()
	*/
	bot.obMerged = orderbook.Merge(bot.obTC, bot.obCB, bot.ctx.TickAndLotTradeBase)
	bot.TryArbTrade()
}
func (bot *Bot) DebugOB(obTB, obCB, obTC *orderbook.Orderbook) {
	bot.obTB = obTB
	bot.obCB = obCB
	bot.obTC = obTC
	bot.obMerged = orderbook.Merge(bot.obTC, bot.obCB, bot.ctx.TickAndLotTradeBase)
}
func (bot *Bot) Loop() {
	log.Print("Bot started")
	log.Printf("Wallet: %s", bot.acc.Wallet)
	debug := false
	if debug {
		bot.debugPath()
		return
	}
	bot.refresh = make(chan struct{})
	for {
		var obTB, obCB, obTC *orderbook.Orderbook
		merge := false
		select {
		case <-bot.ctx.Quit:
			log.Print("Bot stopped")
			return
		case obTB = <-bot.asobTB.Change:
		case obCB = <-bot.asobCB.Change:
			merge = true
		case obTC = <-bot.asobTC.Change:
			merge = true
		}
		if obTB == nil && obCB == nil && obTC == nil {
			continue
		}
		if atomic.CompareAndSwapInt32(&bot.busy, 0, 1) {
			if obTB != nil {
				bot.obTB = obTB
			}
			if obCB != nil {
				bot.obCB = obCB
			}
			if obTC != nil {
				bot.obTC = obTC
			}
			if merge {
				bot.obMerged = orderbook.Merge(bot.obTC, bot.obCB, bot.ctx.TickAndLotTradeBase)
			}
			atomic.CompareAndSwapInt32(&bot.busy, 1, 0)
			go bot.TryArbTrade()
		}
	}
}
func (bot *Bot) readOrderbooks() error {
	var err1, err2, err3 error
	for i := 0; i < 10; i++ {
		bot.obTB, err1 = bot.asobTB.ReadOrderbook()
		bot.obCB, err2 = bot.asobCB.ReadOrderbook()
		bot.obTC, err3 = bot.asobTC.ReadOrderbook()
		if err1 == nil && err2 == nil && err3 == nil {
			break
		}
		time.Sleep(2*time.Second)
	}
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	if err3 != nil {
		return err3
	}
	bot.obMerged = orderbook.Merge(bot.obTC, bot.obCB, bot.ctx.TickAndLotTradeBase)
	return nil
}
func (bot *Bot) TryArbTrade() {
	exe := order.NewExecutor(bot.ctx, bot.acc)
	bot.TryExeArbTrade(exe)
}
func (bot *Bot) TryExeArbTrade(exe order.OrderExecutor) bool {
	if !atomic.CompareAndSwapInt32(&bot.busy, 0, 1) {
		return false
	}
	defer atomic.CompareAndSwapInt32(&bot.busy, 1, 0)

	nilOB := func(ob *orderbook.Orderbook) bool {
		return ob == nil || ob.Bids == nil || ob.Asks == nil || len(ob.Bids) == 0 || len(ob.Asks) == 0
	}
	if nilOB(bot.obTB) || nilOB(bot.obMerged) || nilOB(bot.obCB) || nilOB(bot.obTC) {
		return false
	}
	max := bot.ctx.Config.TradeMax
	arbRes := orderbook.ChekArbOpportunity(bot.obTB, bot.obMerged, max)
	if arbRes == nil {
		log.Printf("No arb opp 1")
		return false
	}
	/*
		// last orderbook scan before arbitrage
		bot.readOrderbooks()
		if nilOB(bot.obTB) || nilOB(bot.obMerged) || nilOB(bot.obCB) || nilOB(bot.obTC) {
			return false
		}
	*/
	bal := bot.acc.GetBal(bot.ctx.Config.BaseAsset)
	if bal < bot.ctx.Config.TradeMax {
		max = bal
	}
	arbRes = orderbook.ChekArbOpportunity(bot.obTB, bot.obMerged, max)
	if arbRes == nil {
		log.Printf("No arb opp 2")
		return false
	}
	log.Printf("YES! Arb opp - wait 1s and try again")
	time.Sleep(1000 * time.Millisecond) // wait if MM makes his trade
	if bot.readOrderbooks() != nil {
		return false
	}
	arbRes = orderbook.ChekArbOpportunity(bot.obTB, bot.obMerged, max)
	if arbRes == nil {
		log.Printf("No arb opp after waiting")
		return false
	}

	log.Printf("\n\n\n\n\n\n\n\n\n\n")
	log.Printf("YES! Still Arb opp")
	log.Printf("\n\n\n\n\n\n\n\n\n\n")
	log.Printf("Wallet balances before arbitrage: %v", bot.acc.GetAllBal())
	{ /*
			// log orederbooks state before arb trade execution - for debug purposes
			dbTrade := db.Trade{}
			dbTrade.Debug = bot.GetTradeDbDebugInfo(arbRes, &orderbook.TripleOrderbookEntry{}, order.PriceVol{}, order.PriceVol{}, order.PriceVol{})
			log.Printf("dbTrade.Debug:\n----------------------\n%+v\n--------------------\n", dbTrade.Debug)
		*/
	}
	/*
		bot.debugLogBotOrderbooks(-1)
		bot.debugReadAndLogOrderbooks(0)
	*/
	//dbTrade := bot.ExecuteArbTrade(exe, arbRes)
	dbTrade := bot.BurstArbTrade(exe, arbRes)
	log.Printf("Account Sequence after arb: %v", bot.acc.Sequence)
	log.Printf("Wallet balances after arbitrage: %v", bot.acc.ReadAllBal())
	log.Printf("Account Sequence after arb - after ReadAllBal: %v", bot.acc.Sequence)
	if dbTrade == nil {
		log.Printf("Arbitrage orders execution failed")
		return true
	}
	if err := db.InsertTrade(bot.ctx, dbTrade); err != nil {
		log.Printf("failed to write arbitrage results into database: %v", err)
	}
	time.Sleep(400 * time.Millisecond)
	if bot.readOrderbooks() != nil {
		return false
	}
	bot.acc.ReadAllBal()
	log.Printf("Account Sequence after arb - after 1 sec - ReadAllBal: %v", bot.acc.Sequence)
	return true
}
func (bot *Bot) fitTradeAmount(am, price1, price2 float64) bool {
	log.Printf("Bot Fit Trade: am >= min : %v >= %v = %v", am, bot.ctx.Config.TradeMin, am >= bot.ctx.Config.TradeMin)
	if price1 > price2 {
		p := price2
		price2 = price1
		price1 = p
	}
	log.Printf("Bot Fit Trade: (p2 - p2) / p2 >= delta (%f - %f) / %f (=%f) >= %f => %v", price2, price1, price2, (price2 - price1) / price2, bot.ctx.Config.MinDelta / 100, (price2 - price1) / price2 >= bot.ctx.Config.MinDelta / 100)
	tradeAmontOk := am >= bot.ctx.Config.TradeMin
	priceDiffOk := (price2 - price1) / price2 >= bot.ctx.Config.MinDelta / 100
	if !tradeAmontOk {
		log.Printf("Warning: arbitrage amount %v < minimum %v", am, bot.ctx.Config.TradeMin)
	}
	if !tradeAmontOk {
		log.Printf("Warning: arbitrage diff %.2f%% < minimum %0.2f%%", (price2 - price1) / price2 * 100, bot.ctx.Config.MinDelta)
	}
	return tradeAmontOk && priceDiffOk
}
func (bot *Bot) ExecuteArbTrade(exe order.OrderExecutor, arbRes *orderbook.ArbitrageOppCheckResult) *db.Trade {
	cx := bot.ctx
	ta := cx.Config.TradedAsset
	ba := cx.Config.BaseAsset
	ca := cx.Config.CrossAsset
	var trade *orderbook.TripleOrderbookEntry
	var pv, pv1, pv2, pv3 order.PriceVol
	dbTrade := &db.Trade{
		Time:  time.Now(),
		Asset: ta,
	}
	if bot.obTB.Bids[0].Price > bot.obMerged.Asks[0].Price { // we buy from obex & sell to ob

		// 1. Order - buy BNB for BUSD
		trade = bot.obMerged.Asks.GetAdjustedValues(cx, arbRes.AskNdx, arbRes.AskLeft)
		if !bot.fitTradeAmount(trade.Amount, bot.obTB.Bids[0].Price, bot.obMerged.Asks[0].Price) { // trade.Amount < cx.Config.TradeMin {
			return nil
		}
		log.Printf("arb trade 1. path - buy %v for %v - > buy %v for %v -> sell %v for %v", ca, ba, ta, ca, ta, ba)
		log.Printf("DEBUG info: trade = %+v", trade)
		pv = order.PriceVol{Price: trade.Ex.Price, Amount: trade.Ex.Amount}
		dbTrade.AmountIn = pv.Amount
		o, err := exe.Execute(ca, ba, pv.Amount, pv.Price, order.Side.BUY) // buy BNB for BUSD
		pv1 = o.GetRes().Pv
		s := fmt.Sprintf("BUY %v. Result: %v", pv.Str(ca, ba), pv1)
		if err != nil {
			log.Printf("Warning / Error: 1/1 arb trade execution failed: %v", s)
			return nil
		}
		dbTrade.AmountIn = pv1.Mul()
		if pv.Amount != pv1.Amount {
			s = s + " - Partially filled"
		}
		log.Printf("1/1 arb trade executed: %v", s)

		//bot.debugReadAndLogOrderbooks(1)

		// 2. Order - buy RUNE for BNB which we just bought
		pv = order.PriceVol{Price: trade.Trade.Price, Amount: ctx.Adjust(ctx.F64xF8F64(pv1.Amount/trade.Trade.Price), cx.TickAndLotTradeCross[1])}
		o, err = exe.Execute(ta, ca, pv.Amount, pv.Price, order.Side.BUY)
		pv2 = o.GetRes().Pv
		s = fmt.Sprintf("BUY %v. Result: %v", pv.Str(ta, ca), pv2)
		if err != nil { // something went wrong, we try to rollback first trade
			log.Printf("Error: 2/1 arb trade failed: %v. Trying to rollback first trade...", s)
			_, err := exe.Execute(ca, ba, pv1.Amount, 0.9*pv1.Price, order.Side.SELL)
			if err != nil {
				log.Printf("2/1 arb trade failed -> prev trade rollback failed")
			} else {
				log.Printf("2/1 arb trade failed -> prev trade rollback successful")
			}
			return nil
		}
		if pv2.Amount != pv.Amount {
			log.Printf("Warning : 2/1 arb trade executed - partialy: %v", s)
		} else {
			log.Printf("2/1 arb trade executed: %v", s)
		}

		//bot.debugReadAndLogOrderbooks(2)

		// 3. Order - sell RUNE for BUSD which we just bought
		pv = order.PriceVol{
			Price:  ctx.Adjust(ctx.F64xF8F64( /*0.9**/ trade.Price), cx.TickAndLotTradeBase[0]),
			Amount: ctx.Adjust(ctx.F64xF8F64(pv2.Amount), cx.TickAndLotTradeBase[1]),
		}
		o, err = exe.Execute(ta, ba, pv.Amount, pv.Price, order.Side.SELL) // sell it
		pv3 = o.GetRes().Pv
		dbTrade.AmountOut = pv3.Mul()
		s = fmt.Sprintf("SELL %v. Result: %v", pv.Str(ta, ba), pv3)
		if err != nil {
			log.Printf("3/1 arb trade failed: %v", s)
			return nil
		}
		log.Printf("3/1 arb trade executed: %v", s)

		//bot.debugReadAndLogOrderbooks(3)

	} else if bot.obMerged.Bids[0].Price > bot.obTB.Asks[0].Price { // we buy from ob & sell to obex

		// 1. Order - buy RUNE for BUSD
		trade = bot.obTB.Asks.GetAdjustedValues(cx, arbRes.AskNdx, arbRes.AskLeft)
		log.Printf("DEBUG info: tradeEx = %+v", trade)
		if !bot.fitTradeAmount(trade.Amount, bot.obTB.Asks[0].Price, bot.obMerged.Bids[0].Price) { // trade.Amount < cx.Config.TradeMin {
			log.Printf("Warning: arbitrage opportunity (%v) is less than the minimum amount: %v", trade.Amount, cx.Config.TradeMin)
			return nil
		}
		log.Printf("arb trade 2. path - buy %v for %v - > sell %v for %v -> sell %v for %v", ta, ba, ta, ca, ca, ba)
		log.Printf("DEBUG info: trade = %+v", trade)
		pv = order.PriceVol{Price: trade.Price, Amount: trade.Amount}
		log.Printf("About to BUY %v", pv.Str(ta, ba))
		o, err := exe.Execute(ta, ba, pv.Amount, pv.Price, order.Side.BUY) // buy RUNE for BUSD
		pv1 = o.GetRes().Pv
		s := fmt.Sprintf("BUY %v. Result: %v", pv.Str(ta, ba), pv1)
		if err != nil {
			log.Printf("Warning / Error: 1/2 arb trade execution failed: %v", s)
			return nil
		}
		dbTrade.AmountIn = pv1.Mul()
		if pv.Amount != pv1.Amount {
			s = s + " - Partially filled"
		}
		log.Printf("1/2 arb trade executed: %v", s)

		//bot.debugReadAndLogOrderbooks(1)

		// 2. Order - sell the RUNE we just bought for BNB
		tradeEx := bot.obMerged.Bids.GetAdjustedValues(cx, arbRes.BidNdx, arbRes.BidLeft)
		log.Printf("DEBUG info: tradeEx = %+v", tradeEx)
		pv = order.PriceVol{Price: tradeEx.Trade.Price, Amount: pv1.Amount}
		o, err = exe.Execute(ta, ca, pv.Amount, pv.Price, order.Side.SELL) // sell RUNE for BNB
		pv2 = o.GetRes().Pv
		s = fmt.Sprintf("SELL %v. Result: %v", pv.Str(ta, ca), pv2)
		if err != nil {
			log.Printf("Error: 2/3 arb trade failed: %v. Trying to rollback the first trade...", s)
			_, err := exe.Execute(ta, ba, pv1.Amount, 0.9*pv1.Price, order.Side.SELL)
			if err != nil {
				log.Printf("2/2 arb trade failed -> prev trade rollback failed")
			} else {
				log.Printf("2/2 arb trade failed -> prev trade rollback successful")
			}
			return nil
		}
		if pv2.Amount != pv.Amount {
			s = s + " - Partially filled"
		}
		log.Printf("2/2 arb trade executed: %v", s)

		//bot.debugReadAndLogOrderbooks(2)

		// 3. Order - sell BNB which we just bought for BUSD
		pv = order.PriceVol{
			Price:  ctx.Adjust(ctx.F64xF8F64( /*0.9**/ tradeEx.Ex.Price), cx.TickAndLotCrossBase[0]),
			Amount: ctx.Adjust(ctx.F64xF8F64(pv2.Amount*tradeEx.Trade.Price), cx.TickAndLotCrossBase[1]),
		}
		o, err = exe.Execute(ca, ba, pv.Amount, pv.Price, order.Side.SELL)
		pv3 = o.GetRes().Pv
		dbTrade.AmountOut = pv3.Mul()
		s = fmt.Sprintf("SELL %v. Result: %v", pv.Str(ca, ba), pv3)
		if err != nil {
			log.Printf("3/2 arb trade failed: %v", s)
			return nil
		}
		log.Printf("3/2 arb trade executed: %v", s)

		//bot.debugReadAndLogOrderbooks(3)

	} else {
		log.Printf("No no no arb opp")
		return nil
	}
	dbTrade.PNL = math.Round(10000.0*(dbTrade.AmountOut-dbTrade.AmountIn)/dbTrade.AmountIn) / 100
	dbTrade.Debug = bot.GetTradeDbDebugInfo(arbRes, trade, pv1, pv2, pv3)
	return dbTrade
}

// read and log curret orederbooks states
func (bot *Bot) debugLogBotOrderbooks(n int) {
	bot.debugLogOrderbooks(n, bot.obTB, bot.obCB, bot.obTC, bot.obMerged)
}

// read and log curret orederbooks states
func (bot *Bot) debugReadAndLogOrderbooks(n int) {
	obTB, _ := bot.asobTB.ReadOrderbook()
	obCB, _ := bot.asobCB.ReadOrderbook()
	obTC, _ := bot.asobTC.ReadOrderbook()
	obMerged := orderbook.Merge(bot.obTC, bot.obCB, bot.ctx.TickAndLotTradeBase)
	bot.debugLogOrderbooks(n, obTB, obCB, obTC, obMerged)
}
func (bot *Bot) debugLogOrderbooks(n int, obTB, obCB, obTC, obMerged *orderbook.Orderbook) {
	log.Printf("-------------------------\n")
	log.Printf("  Debug info - orderbooks after %v-th trade:\n", n)
	log.Print("--------------------\n")
	log.Print("        RUNE / BUSD orderbook:\n")
	bot.debugLogOrderbook(obTB, 3)
	log.Print("--------------------\n")
	log.Print("        BNB / BUSD orderbook:\n")
	bot.debugLogOrderbook(obCB, 3)
	log.Print("--------------------\n")
	log.Print("        RUNE / BNB orderbook:\n")
	bot.debugLogOrderbook(obTC, 3)
	log.Print("--------------------\n")
	log.Print("        RUNE / BUSD merged (RUNE/BNB-BNB/BUSD) orderbook:\n")
	bot.debugLogOrderbook(obMerged, 3)
	log.Print("--------------------\n")
}
func (bot *Bot) debugLogOrderbook(ob *orderbook.Orderbook, depth int) {
	s := "        Asks:"
	for i := 0; i < depth && i < len(ob.Asks); i++ {
		s = fmt.Sprintf("%s %v,", s, order.PriceVol{Price: ob.Asks[i].Price, Amount: ob.Asks[i].Amount})
	}
	log.Println(s)
	s = "        Bids:"
	for i := 0; i < depth && i < len(ob.Bids); i++ {
		s = fmt.Sprintf("%s %v,", s, order.PriceVol{Price: ob.Bids[i].Price, Amount: ob.Bids[i].Amount})
	}
	log.Println(s)
}
func (bot *Bot) BurstArbTrade(exe order.OrderExecutor, arbRes *orderbook.ArbitrageOppCheckResult) *db.Trade {
	cx := bot.ctx
	ta := cx.Config.TradedAsset
	ba := cx.Config.BaseAsset
	ca := cx.Config.CrossAsset
	acc := bot.acc
	var trade *orderbook.TripleOrderbookEntry
	dbTrade := &db.Trade{
		Time:  time.Now(),
		Asset: ta,
	}
	var orders []*order.Order
	if bot.obTB.Bids[0].Price > bot.obMerged.Asks[0].Price { // we buy from obex & sell to ob
		trade = bot.obMerged.Asks.GetAdjustedValues(cx, arbRes.AskNdx, arbRes.AskLeft)
		if !bot.fitTradeAmount(trade.Amount, bot.obTB.Bids[0].Price, bot.obMerged.Asks[0].Price) { //trade.Amount < cx.Config.TradeMin {
			log.Printf("Warning: arbitrage opportunity (%v) is less than the minimum amount: %v", trade.Amount, cx.Config.TradeMin)
			return nil
		}
		log.Printf("arb trade 1. path - buy %v for %v - > buy %v for %v -> sell %v for %v", ca, ba, ta, ca, ta, ba)
		log.Printf("DEBUG info: trade = %+v", trade)
		dbTrade.AmountIn = trade.Ex.Amount * trade.Ex.Price
		orders = []*order.Order{
			order.NewOrder(cx, acc, ca, ba, trade.Ex.Amount, trade.Ex.Price, order.Side.BUY),
			order.NewOrder(cx, acc, ta, ca, trade.Ex.Amount/trade.Trade.Price, trade.Trade.Price, order.Side.BUY),
			order.NewOrder(cx, acc, ta, ba, trade.Ex.Amount/trade.Trade.Price, trade.Price, order.Side.SELL),
		}
	} else if bot.obMerged.Bids[0].Price > bot.obTB.Asks[0].Price { // we buy from ob & sell to obex
		trade = bot.obTB.Asks.GetAdjustedValues(cx, arbRes.AskNdx, arbRes.AskLeft)
		tradeEx := bot.obMerged.Bids.GetAdjustedValues(cx, arbRes.BidNdx, arbRes.BidLeft)
		log.Printf("DEBUG info: tradeEx = %+v", trade)
		if !bot.fitTradeAmount(trade.Amount, bot.obMerged.Asks[0].Price, bot.obTB.Bids[0].Price) { // trade.Amount < cx.Config.TradeMin {
			log.Printf("Warning: arbitrage opportunity (%v) is less than the minimum amount: %v", trade.Amount, cx.Config.TradeMin)
			return nil
		}
		log.Printf("arb trade 2. path - buy %v for %v - > sell %v for %v -> sell %v for %v", ta, ba, ta, ca, ca, ba)
		log.Printf("DEBUG info: trade = %+v", trade)
		dbTrade.AmountIn = trade.Amount * trade.Price
		orders = []*order.Order{
			order.NewOrder(cx, acc, ta, ba, trade.Amount, trade.Price, order.Side.BUY),
			order.NewOrder(cx, acc, ta, ca, trade.Amount, tradeEx.Trade.Price, order.Side.SELL),
			order.NewOrder(cx, acc, ca, ba, trade.Amount*tradeEx.Trade.Price, tradeEx.Ex.Price, order.Side.SELL),
		}
	} else {
		log.Printf("No no no arb opp")
		return nil
	}
	err := exe.BurstExecute(orders)
	if err != nil {
		log.Printf("ERROR: Burst arb trade executed with error: %v", err)
		return nil
	}
	log.Printf("Burst arb trade executed successfuly")

	dbTrade.AmountOut = orders[2].GetRes().Pv.Mul()
	dbTrade.PNL = math.Round(10000.0*(dbTrade.AmountOut-dbTrade.AmountIn)/dbTrade.AmountIn) / 100
	dbTrade.Debug = bot.GetTradeDbDebugInfo(arbRes, trade, orders[0].GetRes().Pv, orders[1].GetRes().Pv, orders[2].GetRes().Pv)
	return dbTrade
}

func (bot *Bot) GetTradeDbDebugInfo(arbRes *orderbook.ArbitrageOppCheckResult, trade *orderbook.TripleOrderbookEntry, pv1, pv2, pv3 order.PriceVol) db.TradeDebug {
	debug := db.TradeDebug{
		ArbRes: db.ArbitrageOppCheckResult{
			BidNdx:  arbRes.BidNdx,
			AskNdx:  arbRes.AskNdx,
			BidLeft: arbRes.BidLeft,
			AskLeft: arbRes.AskLeft,
		},
		PriceVol1: db.SimpleOrderbookEntry(pv1),
		PriceVol2: db.SimpleOrderbookEntry(pv2),
		PriceVol3: db.SimpleOrderbookEntry(pv3),
		Trade:     [3]db.SimpleOrderbookEntry{{trade.Price, trade.Amount}, {trade.Ex.Price, trade.Ex.Amount}, {trade.Trade.Price, trade.Trade.Amount}},
	}
	copyEntries := func(to *[]db.OrderbookEntry, from orderbook.OrderbookEntries) {
		(*to) = make([]db.OrderbookEntry, len(from))
		for i, e := range from {
			(*to)[i].Normal.Price = e.Price
			(*to)[i].Normal.Amount = e.Amount
			(*to)[i].Ex.Price = e.Ex.Price
			(*to)[i].Ex.Amount = e.Ex.Amount
		}
	}
	copyEntries(&debug.Orderbook1.Bids, bot.obTB.Bids)
	copyEntries(&debug.Orderbook1.Asks, bot.obTB.Asks)
	copyEntries(&debug.Orderbook2.Bids, bot.obCB.Bids)
	copyEntries(&debug.Orderbook2.Asks, bot.obCB.Asks)
	copyEntries(&debug.Orderbook3.Bids, bot.obTC.Bids)
	copyEntries(&debug.Orderbook3.Asks, bot.obTC.Asks)
	copyEntries(&debug.OrderbookX.Bids, bot.obMerged.Bids)
	copyEntries(&debug.OrderbookX.Asks, bot.obMerged.Asks)

	return debug
}
