package orderbook

import (
	"log"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/binance-chain/go-sdk/client/websocket"
	types "github.com/binance-chain/go-sdk/common/types"
	"gitlab.com/zlyzol/dexarbot/ctx"
)

type SimpleOrderbookEntry struct {
	Price  float64
	Amount float64
}
type OrderbookEntry struct {
	SimpleOrderbookEntry
	Ex SimpleOrderbookEntry // used only in merged orderbook (cross asset amount)
}
type TripleOrderbookEntry struct {
	OrderbookEntry
	Trade SimpleOrderbookEntry // used only for calculating trade amount and price
}
type OrderbookEntries []OrderbookEntry
type Orderbook struct {
	Bids OrderbookEntries
	Asks OrderbookEntries
}
type AssetOrderbook struct {
	Orderbook
	ctx        *ctx.Context
	asset      string
	depthQuery *types.DepthQuery
	Change     chan *Orderbook
	refresh	   chan struct{}
	mux        sync.Mutex
}
func NewAssetOrderbook(ctx *ctx.Context, asset string) *AssetOrderbook {
	a := AssetOrderbook{
		ctx:    ctx,
		asset:  asset,
		Change: make(chan *Orderbook),
		refresh: make(chan struct{}),
	}
	go a.loop()
	return &a
}
func DebugNewAssetOrderbook(ctx *ctx.Context, asset string) *Orderbook {
	a := AssetOrderbook{
		ctx:    ctx,
		asset:  asset,
		Change: make(chan *Orderbook),
		refresh: make(chan struct{}),
	}
	as := strings.Split(asset, "_")
	a.depthQuery = types.NewDepthQuery(as[0], as[1])
	if a.getOrderbook() {
		return a.getOrderbookCopy()
	} else {
		return nil
	}
}
func (a *AssetOrderbook) subsDepth(resub chan struct{}) {
	as := strings.Split(a.asset, "_")
	a.depthQuery = types.NewDepthQuery(as[0], as[1])
	var err error
	for i := 0; i < 10; i++ {
		log.Printf("call SubscribeMarketDepthEvent for [%v].", a.asset)
		err = a.ctx.Dex.SubscribeMarketDepthEvent(as[0], as[1], a.ctx.Quit, a.onMarketDepthEvent, 
			func(err error) {
				log.Printf("onError - MarketDepthEvent for [%v]. Error: %v", a.asset, err)
				resub <- struct{}{}
			}, 
			func() {
				log.Printf("MarketDepthEvent for [%v] closed", a.asset)
			})
		if err == nil {
			log.Printf("call SubscribeMarketDepthEvent for [%v] sucess", a.asset)
			break
		} else {
			log.Printf("call SubscribeMarketDepthEvent for [%v] failed (%v). Retry", a.asset, err)
		}
		time.Sleep(100*time.Millisecond)
	}
	if err != nil {
		log.Printf("SubscribeMarketDepthEvent for [%v]. No more events. Error: %v", a.asset, err)
		return
	}
}
func (a *AssetOrderbook) subsDiff(resub chan struct{}) {
	as := strings.Split(a.asset, "_")
	a.depthQuery = types.NewDepthQuery(as[0], as[1])
	var err error
	for i := 0; i < 10; i++ {
		log.Printf("call SubscribeMarketDiffEvent for [%v].", a.asset)
		err = a.ctx.Dex.SubscribeMarketDiffEvent(as[0], as[1], a.ctx.Quit, a.onMarketDiffEvent, func(err error) {
				log.Printf("onError - MarketDiffEvent for [%v]. Error: %v", a.asset, err)
				resub <- struct{}{}
			}, 
			func() {
				log.Printf("MarketDiffEvent for [%v] closed", a.asset)
			})
		if err == nil {
			log.Printf("call SubscribeMarketDiffEvent for [%v] success.", a.asset)
			break
		} else {
			log.Printf("call SubscribeMarketDiffEvent for [%v] failed (%v). Retry", a.asset, err)
		}
		time.Sleep(100*time.Millisecond)
	}
	if err != nil {
		log.Printf("SubscribeMarketDiffEvent for [%v]. No more events. Error: %v", a.asset, err)
		return
	}
}
func (a *AssetOrderbook) Refresh() {
	go func() {
		a.refresh <- struct{}{}
	}()
}
func (a *AssetOrderbook) loop() {
	log.Printf("orderbook loop started [%v]", a.asset)
	chDepth := make(chan struct{})
	chDiff := make(chan struct{})
	a.subsDepth(chDepth)
	a.subsDiff(chDiff)
	ticker := time.NewTicker(1000 * time.Millisecond)
	for {
		select {
		case <-a.ctx.Quit:
			log.Printf("orderbook loop stopped [%v] - global Quit", a.asset)
			return
		case <-a.refresh:
			a.get()
		case <-ticker.C:
			//log.Print("orderbook ticket tick")
			a.get()
		case <-chDepth:
			log.Print("Resubscribe Depth on error")
			a.subsDepth(chDepth)
		case <-chDiff:
			log.Print("Resubscribe Diff on error")
			a.subsDiff(chDiff)
		}
	}
}
func (a *AssetOrderbook) getOrderbookCopy() *Orderbook {
	a.mux.Lock()
	defer a.mux.Unlock()
	ob := Orderbook{
		Bids: make([]OrderbookEntry, len(a.Bids)),
		Asks: make([]OrderbookEntry, len(a.Asks)),
	}
	copy(ob.Bids, a.Bids)
	copy(ob.Asks, a.Asks)
	return &ob
}
func (a *AssetOrderbook) get() {
	if a.getOrderbook() {
		a.fireNewOrderbook()
	}
}
func (a *AssetOrderbook) fireNewOrderbook() {
	go func() {
		a.Change <- a.getOrderbookCopy()
	}()
}
func (a *AssetOrderbook) getOrderbook() bool {
	a.mux.Lock()
	defer a.mux.Unlock()

	depth, err := a.ctx.Dex.GetDepth(a.depthQuery)
	if err != nil {
		log.Printf("GetDepth for [%v]. Error: %v", a.asset, err)
		return false
	}
	//log.Printf("GetDepth for [%v].", a.asset)
	newOb := Orderbook{
		Asks: a.GetEntries(depth.Asks, func(a, b float64) bool { return a < b }),
		Bids: a.GetEntries(depth.Bids, func(a, b float64) bool { return a > b }),
	}
	changed := !a.isSubset(&newOb);
	if changed {
		a.Asks = newOb.Asks
		a.Bids = newOb.Bids
	}
	return changed
}
func (a *AssetOrderbook) ReadOrderbook() (*Orderbook, error) {
	a.mux.Lock()
	defer a.mux.Unlock()

	depth, err := a.ctx.Dex.GetDepth(a.depthQuery)
	if err != nil {
		log.Printf("GetDepth for [%v]. Error: %v", a.asset, err)
		return nil, err
	}
	//log.Printf("GetDepth for [%v].", a.asset)
	return &Orderbook{
		Asks: a.GetEntries(depth.Asks, func(a, b float64) bool { return a < b }),
		Bids: a.GetEntries(depth.Bids, func(a, b float64) bool { return a > b }),
	}, nil
}
// isEqual - Orderbook comparision
func (o Orderbook) isEqual(o2 *Orderbook) bool {
	if len(o.Bids) != len(o2.Bids) || len(o.Asks) != len(o2.Asks) {
		return false
	}
	for i, v := range o.Bids {
		if v != o2.Bids[i]{
			return false
		}
	}
	for i, v := range o.Asks {
		if v != o2.Asks[i]{
			return false
		}
	}
	return true
}
// isSubset - true if one orderbook is a subset of the other
func (o Orderbook) isSubset(o2 *Orderbook) bool {
	blen := len(o.Bids); if blen > len(o2.Bids) {blen = len(o2.Bids)}
	alen := len(o.Asks); if alen > len(o2.Asks) {alen = len(o2.Asks)}
	if blen < 10 || alen < 10 {
		return false
	}
	for i := 0; i < blen; i++ {
		if o.Bids[i] != o2.Bids[i] {
			return false
		}
	}
	for i := 0; i < alen; i++ {
		if o.Asks[i] != o2.Asks[i] {
			return false
		}
	}
	return true
}
func (a *AssetOrderbook) onMarketDiffEvent(event *websocket.MarketDeltaEvent) {
	a.mux.Lock()
	defer a.mux.Unlock()
	log.Printf("Market Diff event [%v] Asks: %v, Bids: %v", a.asset, event.Asks, event.Bids)
	if a.Asks == nil || len(a.Asks) == 0 || a.Bids == nil || len(a.Bids) == 0 {
		// wait for full orderbook first
		log.Printf("Market Diff event [%v] - no data - return", a.asset)
		return
	}
	a.Asks = a.UpdateEntriesF8(a.Asks, event.Asks, func(a, b float64) bool { return a < b })
	a.Bids = a.UpdateEntriesF8(a.Bids, event.Bids, func(a, b float64) bool { return a > b })
	log.Printf("Market Diff event [%v] - updated obs - fireNewOrderbook", a.asset)
	a.fireNewOrderbook()
	log.Printf("Market Diff event [%v] - updated obs - fireNewOrderbook - done", a.asset)
}
func (a *AssetOrderbook) onMarketDepthEvent(event *websocket.MarketDepthEvent) {
	a.mux.Lock()
	defer a.mux.Unlock()
	//log.Printf("Market Depth event [%v]", a.asset)
	ob := Orderbook{}
	ob.Asks = a.GetEntriesF8(event.Asks, func(a, b float64) bool { return a < b })
	ob.Bids = a.GetEntriesF8(event.Bids, func(a, b float64) bool { return a > b })
	if !a.isSubset(&ob) {
		/*
		log.Printf("Market Depth event [%v] -> orderbook changed", a.asset)
		if a.Bids == nil {
			log.Printf("    [%v] bid / ask old : %v / %v      new : %v / %v", a.asset, "nil", "nil", ob.Bids[0].Price, ob.Asks[0].Price, )
		} else {
			log.Printf("    [%v] old (%v)(%v) bid / ask: %v / %v      new (%v)(%v): %v / %v", a.asset, len(a.Bids), len(a.Asks), a.Bids[0].Price, a.Asks[0].Price, len(ob.Bids), len(ob.Asks), ob.Bids[0].Price, ob.Asks[0].Price, )
		}
		*/
		a.Bids = ob.Bids
		a.Asks = ob.Asks
		a.fireNewOrderbook()
	} else {
		 //log.Printf("Market Depth event [%v] -> no change", a.asset)
	}
}
func (a *AssetOrderbook) GetEntriesF8(entries [][]types.Fixed8, compare func(float64, float64) bool) []OrderbookEntry {
	ob := make([]OrderbookEntry, len(entries))
	for i, pv := range entries {
		ob[i] = OrderbookEntry{}
		ob[i].Price = ctx.F8xF64(pv[0])
		ob[i].Amount = ctx.F8xF64(pv[1])
	}
	sort.SliceStable(ob, func(i, j int) bool {
		return compare(ob[i].Price, ob[j].Price)
	})
	return ob
}
func (a *AssetOrderbook) GetEntries(entries [][]string, compare func(float64, float64) bool) OrderbookEntries {
	ob := make([]OrderbookEntry, len(entries))
	for i, pv := range entries {
		ob[i] = OrderbookEntry{}
		ob[i].Price = ctx.F8xF64(ctx.SxF8(pv[0]))
		ob[i].Amount = ctx.F8xF64(ctx.SxF8(pv[1]))
	}
	sort.SliceStable(ob, func(i, j int) bool {
		return compare(ob[i].Price, ob[j].Price)
	})
	return ob
}
func (a *AssetOrderbook) UpdateEntriesF8(old OrderbookEntries, new [][]types.Fixed8, compare func(float64, float64) bool) OrderbookEntries {
	add := make(OrderbookEntries, 0, 20)
	for _, ne := range new {
		np := ctx.F8xF64(ne[0])
		na := ctx.F8xF64(ne[1])
		i := sort.Search(len(old), func(i int) bool { return old[i].Price >= np })
		if i < len(old) && old[i].Price == np {
			old[i].Amount = na
		} else {
			ob := OrderbookEntry{}
			ob.Price = np
			ob.Amount = na
			add = append(add, ob)
		}
	}
	mix := make(OrderbookEntries, 0, len(old) + len(add))
	for _, e := range old {
		if e.Amount != 0 {
			mix = append(mix, e)
		}
	}
	for _, e := range add {
		if e.Amount != 0 {
			mix = append(mix, e)
		}
	}
	sort.SliceStable(mix, func(i, j int) bool {
		return compare(mix[i].Price, mix[j].Price)
	})
	return mix
}
// GetAdjustedValues - sums all traded OB entries's amounts, adjust it by coefficient (k) and adjusts the resulting sums and prices by corresponding Lots and Ticks
func (es OrderbookEntries) GetAdjustedValues(cx *ctx.Context, i int, k float64) *TripleOrderbookEntry {
	oe := TripleOrderbookEntry{}
	for j := 0; j < i; j++ {
		oe.Amount, oe.Ex.Amount, oe.Trade.Amount = oe.Amount+es[j].Amount, oe.Ex.Amount+es[j].Ex.Amount, oe.Amount+es[j].Amount
	}
	oe.Amount, oe.Ex.Amount, oe.Trade.Amount = ctx.F64xF8F64(oe.Amount+k*es[i].Amount), ctx.F64xF8F64(oe.Ex.Amount+k*es[i].Ex.Amount), ctx.F64xF8F64(oe.Trade.Amount+k*es[i].Amount)
	oe.Price, oe.Ex.Price = ctx.F64xF8F64(es[i].Price), ctx.F64xF8F64(es[i].Ex.Price)
	if es[i].Ex.Price == 0 {
		oe.Trade.Price = 0
	} else {
		oe.Trade.Price = ctx.F64xF8F64(es[i].Price/es[i].Ex.Price)
	}
	oe.Price, oe.Ex.Price, oe.Trade.Price = ctx.Adjust(oe.Price, cx.TickAndLotTradeBase[0]), ctx.Adjust(oe.Ex.Price, cx.TickAndLotCrossBase[0]), ctx.Adjust(oe.Trade.Price, cx.TickAndLotTradeCross[0])
	oe.Amount, oe.Ex.Amount, oe.Trade.Amount = ctx.Adjust(oe.Amount, cx.TickAndLotTradeBase[1]), ctx.Adjust(oe.Ex.Amount, cx.TickAndLotCrossBase[1]), ctx.Adjust(oe.Trade.Amount, cx.TickAndLotTradeCross[1])
	return &oe
}
