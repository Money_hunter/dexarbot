package ctx

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"time"
	"strconv"

	"github.com/binance-chain/go-sdk/client"
	types "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)
type Config struct {
	Wallet struct {
		Keystore string `json:"keystore"`
		Password string `json:"password"`
	} `json:"wallet"`
	LogFile     string  `json:"logfile"`
	ListenPort	string	`json:"listen_port"`
	TradeMin    float64 `json:"trademin"`
	TradeMax    float64 `json:"trademax"`
	MinDelta	float64	`json:"mindeltaperc"`
	Reserve     float64 `json:"reserve"`
	BaseAsset   string  `json:"baseasset"`
	TradedAsset string  `json:"tradedasset"`
	CrossAsset  string  `json:"crossasset"`
}

type Context struct {
	Config               *Config
	TickAndLotTradeBase  [2]uint64        // Tick and Lot for RUNE_BUSD
	TickAndLotTradeCross [2]uint64        // Tick and Lot for RUNE_BNB
	TickAndLotCrossBase  [2]uint64        // Tick and Lot for BNB_USD
	Key                  keys.KeyManager  // Binance DEX wallet
	Dex                  client.DexClient // Binance DEX client
	Db                   *mongo.Client
	Quit                 chan struct{}
}

func NewConfig(file string) *Config {
	var cfg Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		log.Fatalf("Error: Config file open failed: %v", err)
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&cfg)
	if err != nil {
		log.Fatalf("Error: Wrong Config file format: %v", err)
	}
	// logging init
	logFilename := cfg.LogFile + "_" + time.Now().Format("20060102_150405")
	logFile, err := os.OpenFile(logFilename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Error: Cannot open log file [%v]. Error: %v", cfg.LogFile, err)
	}
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)
	return &cfg
}

func New(configFile string) *Context {
	ctx := Context{}
	if configFile == "" {
		configFile = "config.json"
	}
	// Config init
	ctx.Config = NewConfig(configFile)
	// keys & address
	var err error
	ctx.Key, err = keys.NewKeyStoreKeyManager(ctx.Config.Wallet.Keystore, ctx.Config.Wallet.Password)
	if err != nil {
		log.Fatalf("Error: Keysore file open failed: %v", err)
	}
	// create Binance DEX Client
	ctx.Dex, err = client.NewDexClient("dex.binance.org", types.ProdNetwork, ctx.Key)
	if err != nil {
		log.Fatalf("Error: Bnance DEX client connect failed: %v", err)
	}
	// get Tick and Lot fer each assets
	err = ctx.GetTickAndLot()
	if err != nil {
		log.Fatalf("Error: GetTickAndLot failed: %v", err)
	}
	// connect to DB
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	ctx.Db, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatalf("Error: MongoDB connect failed: %v", err)
	}
	err = ctx.Db.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatalf("Error: MongoDB ping failed: %v", err)
	}
	log.Print("Connected to MongoDB!")

	return &ctx
}
func (ctx *Context) Close() {
	ctx.Db.Disconnect(context.TODO())
	log.Print("Connection to MongoDB closed.")
}
func (ctx *Context) Start() {
	ctx.Quit = make(chan struct{})
}
func (ctx *Context) Stop() {
	close(ctx.Quit)
}
func F8xF64(f8 types.Fixed8) float64 {
	return float64(f8) / float64(types.Fixed8Decimals)
}
func F64xI64(f float64) int64 {
	return int64(math.Round(f * float64(types.Fixed8Decimals)))
}
func F64xF8F64(f float64) float64 {
	return math.Round(f*float64(types.Fixed8Decimals)) / float64(types.Fixed8Decimals)
}
// SxF8 converts string to Fixed8 (* 1e8)
func SxF8(s string) types.Fixed8 {
	v, err := types.Fixed8DecodeString(s)
	if nil != err {
		log.Printf("Parse error DEX string Fixed8 number [%v]", s)
		return 0
	}
	return v
}
// S8xF64 converts string (expecting Fixed8 float) to float64
func S8xF64(s string) float64 {
	f, err := strconv.ParseFloat(s, 64)
	if nil != err {
		log.Printf("Parse error DEX string Fixed8 float number [%v]", s)
		return 0
	}
	return f
}
// Balances - account balances
type Balances map[string]float64
// Account - binance chain account
type Account struct {
	Balances 	Balances
	Number		int64
	Sequence	int64
	Wallet 		string
	Bech32		types.AccAddress
	ctx 		*Context
}
func (ctx *Context) GetAcc() *Account {
	acc := Account{ctx: ctx}
	acc.Bech32 = ctx.Dex.GetKeyManager().GetAddr()
	acc.Wallet = acc.Bech32.String()
	acc.ReadBalances()
	return &acc
}
func (a *Account) GetBal(asset string) float64{
	if a == nil || a.Balances == nil {
		return 0
	}
	if v, ok := a.Balances[asset]; ok {
		return v
	}
	return 0
}
func (a *Account) GetAllBal() *Balances{
	if a == nil || a.Balances == nil {
		return &Balances{}
	}
	return &a.Balances
}
func (a *Account) ReadAllBal() *Balances{
	a.ReadBalances()
	return &a.Balances
}
func (acc *Account) ReadBalances() {
	balance, err := acc.ctx.Dex.GetAccount(acc.Wallet)
	if err != nil {
		log.Printf("ERROR: GetAccount call failed: %v", err)
		return 
	}
	acc.Balances = make(Balances, len(balance.Balances))
	for _, coin := range balance.Balances {
		acc.Balances[coin.Symbol] = F8xF64(coin.Free)
	}
	acc.Number = balance.Number
	acc.Sequence = balance.Sequence
	log.Printf("INFO: ReadBalances (%v): %v", acc.Wallet, acc.Balances)
	return 
}
func (ctx *Context) GetTickAndLot() error {
	markets, err := ctx.Dex.GetMarkets(types.NewMarketsQuery().WithLimit(1000))
	if err != nil {
		return fmt.Errorf("ERROR: GetMarkets call failed: %v", err)
	}
	cnt := 3
	for _, pair := range markets {
		switch pair.BaseAssetSymbol + "_" + pair.QuoteAssetSymbol {
		case ctx.Config.TradedAsset + "_" + ctx.Config.BaseAsset:
			ctx.TickAndLotTradeBase = [2]uint64{uint64(pair.TickSize), uint64(pair.LotSize)}
			cnt--
		case ctx.Config.TradedAsset + "_" + ctx.Config.CrossAsset:
			ctx.TickAndLotTradeCross = [2]uint64{uint64(pair.TickSize), uint64(pair.LotSize)}
			cnt--
		case ctx.Config.CrossAsset + "_" + ctx.Config.BaseAsset:
			ctx.TickAndLotCrossBase = [2]uint64{uint64(pair.TickSize), uint64(pair.LotSize)}
			cnt--
		}
		if cnt == 0 {
			break
		}
	}
	if cnt != 0 {
		return fmt.Errorf("ERROR: GetTickAndLot failed. Some asset not found. Expected 3, found %v", (1 - cnt))
	}
	return nil
}

func Adjust(val float64, tickLot uint64) float64 {
	v1 := math.Round(val * float64(types.Fixed8Decimals))
	v2 := math.Round(v1 / float64(tickLot)) * float64(tickLot)
	return v2 / float64(types.Fixed8Decimals)
}
func (ctx *Context) AdjustTickAndLot(asset string, price, amount float64) (adjPrice float64, adjAmount float64) {
	adjPrice, adjAmount = price, amount
	switch asset {
	case ctx.Config.TradedAsset + "_" + ctx.Config.BaseAsset:
		adjPrice = Adjust(price, ctx.TickAndLotTradeBase[0])
		adjAmount = Adjust(amount, ctx.TickAndLotTradeBase[1])
	case ctx.Config.TradedAsset + "_" + ctx.Config.CrossAsset:
		adjPrice = Adjust(price, ctx.TickAndLotTradeCross[0])
		adjAmount = Adjust(amount, ctx.TickAndLotTradeCross[1])
	case ctx.Config.CrossAsset + "_" + ctx.Config.BaseAsset:
		adjPrice = Adjust(price, ctx.TickAndLotCrossBase[0])
		adjAmount = Adjust(amount, ctx.TickAndLotCrossBase[1])
	default:
	}
	return adjPrice, adjAmount
}
